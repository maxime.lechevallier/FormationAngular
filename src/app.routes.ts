import { Routes } from '@angular/router';
import { QcmListComponent } from './qcm-list.component';
import { QcmEditComponent } from './qcm-edit.component';

export const ROUTES: Routes = [
    { path: '', component: QcmListComponent},
    { path: 'edit', component: QcmEditComponent}
];