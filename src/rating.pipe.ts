import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'rating'})
export class RatingPipe implements PipeTransform {
    transform(value, args){
        let stars: string = '';
        for (let i = 0; i < value; i++) {
            stars += '★';
        }
        return stars;
    }
}