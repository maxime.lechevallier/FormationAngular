import { Component, Input, Output, EventEmitter } from "@angular/core";
import { QCM } from "./qcm.interface"
import { Question } from "./question.interface"


@Component({
    selector: 'qcm-cmp',
    template: `
        <h2>{{qcm.name}} ({{qcm.level | rating}})</h2>
        <span>{{qcm.questions.length}} questions</span>

        <question-cmp 
            *ngIf="!done"
            [question]="qcm.questions[qcm.currentQuestion]"
            [done]="qcm.done"
            (answerSelected)="incQuestion($event)">
        </question-cmp>
        <button *ngIf="qcm.currentQuestion" (click)="decQuestion()">previous</button>
        <button *ngIf="qcm.currentQuestion < qcm.questions.length - 1" (click)="incQuestion(null)">next</button>
        <button *ngIf="!qcm.done && isAllAnswered()" (click)="endQcm()">evaluate</button>
        <div *ngIf="qcm.done">Result: {{qcm.score}} answers correct out of {{qcm.questions.length}}</div>
    `,
})
export class QcmComponent {

    @Input() qcm: QCM;

    decQuestion(answerSelected){
        this.qcm.currentQuestion--;
    }

    incQuestion(answerSelected){
        if(!this.qcm.done){
            this.qcm.questions[this.qcm.currentQuestion].answerSelected = answerSelected;
        }
        if(this.qcm.currentQuestion < this.qcm.questions.length - 1){
            this.qcm.currentQuestion++;
        }
    }

    isAllAnswered(){
        return this.qcm.questions.every(question => {
            return question.answerSelected != null;
        });
    }

    endQcm() {
        this.setScore();
        this.qcm.done = true;
    }

    setScore(){
        this.qcm.score = this.calculResult();
    }

    
    calculResult() : number {
        let score : number = 0;
        this.qcm.questions.forEach(question => {
            question.rightAnswers.forEach(i => {
                score += (question.answerSelected === question.answers[i]) ? 1 : 0; 
            });
        });
        
        return score;
    }

}