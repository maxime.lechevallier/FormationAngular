import { Injectable, EventEmitter } from '@angular/core'
import 'rxjs/add/observable/of';
import { Form } from "./form.interface"
import { QCM } from "./qcm.interface"
import { Question } from "./question.interface"
import { Answer } from "./answer.interface"
import { Observable } from "rxjs/Observable";

@Injectable()
export class QcmService {

    qcms : Array<QCM> = new Array<QCM>();
    answers : Array<Answer> = new Array<Answer>();

    constructor(){
        this.answers = [
                            {'text' : '42'},
                            {'text' : 'la réponse d'},
                            {'text' : 'parceque'},
                            {'text' : 'noui'},
                        ];
        this.qcms.push(
            {
                'name' :'Javascript',
                'level':2,
                'questions':                     
                    [{
                        'entitled' : 'Pourquoi?',
                        'answers' : this.answers,
                        'answerSelected' : null,
                        'rightAnswers' : [0, 1],
                        
                    },
                    {
                        'entitled' : 'Comment?',
                        'answers' : this.answers,
                        'answerSelected' : null,
                        'rightAnswers' : [0],                      
                    },],
                'currentQuestion': 0,
                'score' : -1,
                'done': false  
            },
            {
                'name' :'HTML 5',
                'level':1,
                'questions':                      
                    [{
                        'entitled' : 'Pourquoi?',
                        'answers' : this.answers,
                        'answerSelected' : null,
                        'rightAnswers' : [0, 1],
                        
                    },
                    {
                        'entitled' : 'Comment?',
                        'answers' : this.answers,
                        'answerSelected' : null,
                        'rightAnswers' : [0],                      
                    },],
                'currentQuestion': 0,
                'score' : -1,
                'done': false   
            },
            {
                'name' :'Angular',
                'level':5,
                'questions':                     
                    [{
                        'entitled' : 'Pourquoi?',
                        'answers' : this.answers,
                        'answerSelected' : null,
                        'rightAnswers' : [0, 1],
                        
                    },
                    {
                        'entitled' : 'Comment?',
                        'answers' : this.answers,
                        'answerSelected' : null,
                        'rightAnswers' : [0],                      
                    },],
                'currentQuestion': 0,
                'score' : -1,
                'done': false  
            },
        );
    }

    getQcms() : Observable<Array<QCM>>{
        return Observable.of(this.qcms);
    }

    deleteQcm(qcmToDelete : QCM) {
        for(var i = this.qcms.length -1; i >= 0 ; i--){
            if(this.qcms[i] == qcmToDelete){
                this.qcms.splice(i, 1);
            }
        }
    }

    addQcm(qcmForm : Form){
        this.qcms.push(
        {
            'name' :qcmForm.qcmName,
            'level':qcmForm.qcmName.length,
            'questions':                     
                [{
                    'entitled' : 'Pourquoi?',
                    'answers' : this.answers,
                    'answerSelected' : null,
                    'rightAnswers' : [0, 1],
                    
                },
                {
                    'entitled' : 'Comment?',
                    'answers' : this.answers,
                    'answerSelected' : null,
                    'rightAnswers' : [0],                      
                },],
            'currentQuestion': 0,
            'score' : -1,
            'done': false  
        });
    }
}