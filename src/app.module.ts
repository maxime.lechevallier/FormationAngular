import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ROUTES } from "./app.routes";
import { AppComponent } from "./app.component";
import { QcmListComponent } from "./qcm-list.component";
import { QcmEditComponent } from "./qcm-edit.component";
import { QcmComponent } from "./qcm.component";
import { QuestionComponent } from "./question.component";
import { AnswerComponent } from "./answer.component";
import { QcmService } from "./qcm.service";
import { RatingPipe } from "./rating.pipe";
import { ConteneurComponent } from "./conteneur.component";



@NgModule({
    imports: [BrowserModule, FormsModule, RouterModule.forRoot(ROUTES), BrowserAnimationsModule],
    declarations: [AppComponent, ConteneurComponent, QcmEditComponent, QcmListComponent, QcmComponent, QuestionComponent, AnswerComponent, RatingPipe],
    providers: [QcmService],
    bootstrap: [AppComponent]
})
export class AppModule{

}