import { trigger, state, style, transition, animate } from "@angular/animations";

export const shift = trigger('shift', [
    state('expand', style({
        width : 100,
        fill : "red",
        rx : 50,
        ry : 50,
    })),
    state('collapse', style({
        width : 500,
        fill : "gray",
        rx : 0,
        ry : 0,
    })),
    transition('expand => collapse', animate(500)),
    transition('collapse => expand', animate(500))
])