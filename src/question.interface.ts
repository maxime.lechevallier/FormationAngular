import { Answer } from "./answer.interface";

export interface Question{
    entitled : String;
    answers: Array<Answer>;
    answerSelected: Answer;
    rightAnswers: Array<number>;
};