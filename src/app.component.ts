import { Component } from "@angular/core";


@Component({
    selector: 'qcm-app',
    template: `
        <conteneur-app></conteneur-app>
        <a routerLink="/">List</a>
        <a routerLink="/edit">Edit</a>        
        <router-outlet></router-outlet>
    `,
})
export class AppComponent {



}