import { Question } from "./question.interface";

export interface QCM{
    name : String;
    level: number;
    questions: Array<Question>;
    currentQuestion: number,
    score: number;
    done: boolean;
};