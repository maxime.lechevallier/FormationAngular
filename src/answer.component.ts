import { Component, Input } from "@angular/core";
import { Answer } from "./answer.interface"


@Component({
    selector: 'answer-cmp',
    template: `
        <p>{{answer.text}}</p>
    `,
})
export class AnswerComponent {

    @Input() answer: Answer;

}