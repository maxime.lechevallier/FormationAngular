import { Component, Input, OnDestroy } from "@angular/core";
import { Form } from "./form.interface"
import { QCM } from "./qcm.interface"
import { QcmService } from "./qcm.service";
import { Subscription } from "rxjs/Subscription";


@Component({
    selector: 'qcm-edit-app',
    styles:[`input.ng-invalid{ border: 5px red solid;} input.ng-valid{ border: 5px green solid;}`],
    template: `
        <h2>Edition</h2>
        <ul>
            <li *ngFor="let qcm of qcms" >
                {{qcm.name}} ({{qcm.level | rating}})
                <button (click)=deleteQcm(qcm)>Delete</button>
            </li>
            <button (click)="revealForm=true">Add</button>
            <form *ngIf=revealForm (ngSubmit)="addQcm(qcmForm.value)" #qcmForm="ngForm">
                <label>QCM: </label>
                <input name="qcmName" ngModel required minlength="3" #qcmName="ngModel">
                <div *ngIf="qcmName.control.dirty && !qcmName.control.valid">Qcm name is required</div>
                <button type="submit" [disabled]="!qcmForm.control.valid">submit</button>
            </form>
        </ul>
    `,
})
export class QcmEditComponent implements OnDestroy {

    qcms : Array<QCM>;
    currentQcm : QCM;
    private subscsription : Subscription;

    constructor(private qcmService : QcmService){
        this.subscsription = this.qcmService.getQcms().subscribe(qcms => this.qcms = qcms);
    }

    ngOnDestroy(){
        this.subscsription.unsubscribe();
    }

    deleteQcm(qcm : QCM) : void {
        this.qcmService.deleteQcm(qcm);
    }

    addQcm(qcmForm: Form) : void {
        this.qcmService.addQcm(qcmForm);
    }


}