import { Component, Input, OnDestroy } from "@angular/core";
import { QCM } from "./qcm.interface"
import { QcmService } from "./qcm.service";
import { Subscription } from "rxjs/Subscription";


@Component({
    selector: 'qcm-list-app',
    styles: [`.done{ color:gray };`],
    template: `
        <ul>
            <li *ngFor="let qcm of qcms" (click)="setCurrentQcm(qcm)" [ngClass]="setClass(qcm)">
                {{qcm.name}} ({{qcm.level | rating}})
            </li>
        </ul>
        <qcm-cmp *ngIf="currentQcm" [qcm]="currentQcm"></qcm-cmp>
    `,
})
export class QcmListComponent implements OnDestroy {

    qcms : Array<QCM>;
    currentQcm : QCM;
    private subscsription : Subscription;

    constructor(private qcmService : QcmService){
        this.subscsription = this.qcmService.getQcms().subscribe(qcms => this.qcms = qcms);
    }

    ngOnDestroy(){
        this.subscsription.unsubscribe();
    }
    

    setClass(qcm : QCM) : String{
        if(qcm.done){
            return "done";
        }
    }

    setCurrentQcm(qcmSelected){
        this.currentQcm = qcmSelected;
    }
}