import { AnimationEvent } from "@angular/animations";
import { Component } from "@angular/core";
import { shift } from "./expand.animation";


@Component({
    selector: 'conteneur-app',
    animations: [shift],
    template: `
        <svg style="text-align:center;" width="400" height="200">
            <g [@shift]="state">
                <rect x="0" y="0" width="400" height="100" fill="red" (click)="changeState()" [@shift]="state"></rect>
                <text x="0" y="50" font-family="Verdana" font-size="35" fill="blue" style="pointer-events: none;">QCM App</text>
                <text x="0" y="100" font-family="Verdana" font-size="35" fill="blue" style="pointer-events: none;">lorem {{state}}</text>
            </g>
        </svg>
    `,
})
export class ConteneurComponent {
    state : string = "collapse";

    changeState() : void{
        if(this.state == "expand"){
            this.state = "collapse";
        }else{
            this.state = "expand";
        }
    }

}