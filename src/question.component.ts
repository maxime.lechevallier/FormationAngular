import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Question } from "./question.interface"
import { Answer } from "./answer.interface"


@Component({
    selector: 'question-cmp',
    styles: [`.wrong{ background-color: red }
             .correct{ background-color: green }`],
    template: `
        <h3 [textContent]="question.entitled"></h3>
        <ul>
            <li *ngFor="let answer of question.answers; let i = index" (click)=selectAnswer(answer)>
                <p [ngClass]="setClass(i)">
                    <answer-cmp [answer]="answer"></answer-cmp>
                </p>
            </li>
        </ul>
    `,
})
export class QuestionComponent {

    @Input() question: Question;
    @Input() done: Boolean;
    @Output() answerSelected : EventEmitter<Answer> = new EventEmitter<Answer>();

    selectAnswer(answer){
        this.answerSelected.emit(answer);
    }
    
    setClass(i : number) : String{
        if(this.done){
            let isCorrect : boolean = false;
            this.question.rightAnswers.forEach(j => {
                if(i === j){
                    isCorrect = true;
                }
            });
            if(this.question.answerSelected === this.question.answers[i] && !isCorrect)
                return "wrong"
            if(isCorrect)
                return "correct";
        }
    }

}